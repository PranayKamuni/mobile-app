package com.learn.mobileappws.git;

public class SwapNumbers {
    public static void main(String args[])
    {
    int number1 = 11, number2 = 25;
    System.out.println("numbers before swapping");
		System.out.println("Number1 : "+ number1);
		System.out.println("Number2 : "+ number2);
    int number3;
    number3=number2;
    number2=number1;
    number1=number3;
		System.out.println("Numbers after swap");
		System.out.println("Number1 : "+ number1);
		System.out.println("Number2 : "+ number2);
}
}