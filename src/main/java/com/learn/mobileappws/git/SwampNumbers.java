package com.learn.mobileappws.git;

public class SwampNumbers {

    public static void main(String args[]) {
        int numberOne = 11, numberTwo = 25;
        System.out.println("numbers before swapping");
        System.out.println("Number1 : " + numberOne);
        System.out.println("Number2 : " + numberTwo);
        int numberThree;
        numberThree = numberTwo;
        numberTwo = numberOne;
        numberOne = numberThree;
        System.out.println("Numbers after swap");
        System.out.println("Number1 : " + numberOne);
        System.out.println("Number2 : " + numberTwo);
    }
}