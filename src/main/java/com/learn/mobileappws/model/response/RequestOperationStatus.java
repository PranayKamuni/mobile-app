package com.learn.mobileappws.model.response;

/**
 * @author vkongara
 */
public enum RequestOperationStatus {
    ERROR, SUCCESS

}
