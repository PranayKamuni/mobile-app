package com.learn.mobileappws.io.repository;

import com.learn.mobileappws.io.entity.*;
import org.springframework.data.repository.*;
import org.springframework.stereotype.Repository;

/**
 * @author vkongara
 */
/**
 * CrudRepository is replaced PagingAndSortingRepository to get use of pagination
 */
@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {
    UserEntity findByUserId(String id);

}
