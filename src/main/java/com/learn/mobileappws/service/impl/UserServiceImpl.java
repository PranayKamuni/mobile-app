package com.learn.mobileappws.service.impl;

import com.learn.mobileappws.dao.UserDao;
import com.learn.mobileappws.io.entity.UserEntity;
import com.learn.mobileappws.io.repository.UserRepository;
import com.learn.mobileappws.service.UserService;
import com.learn.mobileappws.shared.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.awt.print.*;
import java.util.*;

/**
 * @author vkongara
 */
@Service

public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    Utils utils;


    @Override
    public UserDao createUser(UserDao user) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(user, userEntity);

        userEntity.setEncryptedPassword("test");
        String publicUserId = utils.generateUserId(4);
        userEntity.setUserId(publicUserId);
        UserEntity storedUserDetails = userRepository.save(userEntity);

        UserDao returnValue = new UserDao();
        BeanUtils.copyProperties(storedUserDetails, returnValue);

        return returnValue;
    }

    @Override
    public UserDao getUserByUserId(String userId) {
        UserDao returnValue = new UserDao();
        UserEntity userEntity = userRepository.findByUserId(userId);
        BeanUtils.copyProperties(userEntity, returnValue);

        return returnValue;
    }

    @Override
    public UserDao updateUser(String userId, UserDao user) {
        UserDao returnValue = new UserDao();
        UserEntity userEntity = userRepository.findByUserId(userId);
        userEntity.setFirstName(user.getFirstName());
        userEntity.setLastName(user.getLastName());
        UserEntity updateddUserDetails = userRepository.save(userEntity);
        BeanUtils.copyProperties(updateddUserDetails, returnValue);
        return returnValue;
    }

    @Override
    public void deleteUserByUserId(String userId) {
        UserEntity userEntity = userRepository.findByUserId(userId);
        userRepository.delete(userEntity);
    }

    @Override
    public List<UserDao> getUsers(int page, int limit) {
        List<UserDao> returnValue = new ArrayList<>();
        Pageable pageableRequest = PageRequest.of(page, limit);
        Page<UserEntity> userPage = userRepository.findAll(pageableRequest);
        List<UserEntity> users = userPage.getContent();

        for (UserEntity userEntity : users) {
            UserDao userDao = new UserDao();
            BeanUtils.copyProperties(userEntity, userDao);
            returnValue.add(userDao);
        }
        return returnValue;
    }


}
