package com.learn.mobileappws.service;

import com.learn.mobileappws.dao.UserDao;

import java.util.*;


/**
 * @author vkongara
 */
public interface UserService {
    UserDao createUser(UserDao user);

    UserDao getUserByUserId(String userId);

    UserDao updateUser(String userId, UserDao user);

    void deleteUserByUserId(String userId);

    List<UserDao> getUsers(int page, int limit);

}
